package co.uk.zadania_lukasz_001;
import java.util.Scanner;

/**
 1.Przygotuj program, który obiera od użytkownika 3 liczby, a następnie wskazuje największą oraz najmniejszą z nich.
 Dodatkow oprogram oblicza sumę podanych liczb.
 2.Zaktualizuj program, aby umożliwiał użytkownikowi przekazanie dowolnej liczby parametrów.
 */



public class MaxMin {

    public static int[] maxmin() {

        Scanner scanner = new Scanner(System.in);
        System.out.println("Ile liczb chcesz porownac?");
        int size = scanner.nextInt();
        System.out.println("Proszę podać " +size + " liczb(y)");
        int[] numbers = new int[size];
        int suma = 0;
        for (int i = 0; i < numbers.length; i++) {
            numbers[i] = scanner.nextInt();
            suma += numbers[i];
        }

        int[] result = new int[2];
        result[0] = findMax(numbers);
        result[1] = findMin(numbers);

        System.out.println(String.format("Suma wszystkich wprowadzonych liczb to %d. Najwieksza z wprowadzonych liczb to: %d , najmniejsza to %d", suma, result[0], result[1]));


        return result;
    }


    public static int findMax(int[] values) {
        int max = Integer.MIN_VALUE;
        for (int value : values) {
            if (value > max) {
                max = value;
            }
        }
        return max;
    }


    public static int findMin(int[] values) {
        int min = Integer.MAX_VALUE;
        for (int value : values) {
            if (value < min) {
                min = value;
            }
        }
        return min;
    }



    public static void main(String[] args) {

        maxmin();

    }

}
