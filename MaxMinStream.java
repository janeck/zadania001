package co.uk.zadania_lukasz_001;

import java.util.ArrayList;
import java.util.Random;

/**
 6.Wylosuj 1000 liczbz zakresu 1000-2000 i zapisz je do listy. Skorzystaj z stream() w liście do wyszukania
 najmniejszej i największej wartości.
 */

public class MaxMinStream {
    public static void main(String[] args) {
    Random random = new Random();
    ArrayList<Integer> randomthousand = new ArrayList<Integer>();
        for (int i = 0; i < 1000 ; i++) {
            randomthousand.add(random.nextInt(1000) + 1000);
        }


int max = randomthousand.stream().mapToInt(n->n).max().getAsInt();
int min = randomthousand.stream().mapToInt(n->n).min().getAsInt();
System.out.printf("Najwieksza wylosowana liczba to %d, Najmniejsza wylosowana liczba to %d", max,min);

    }
}
