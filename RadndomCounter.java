package co.uk.zadania_lukasz_001;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Random;

/**
 * Przygotuj program, który losuje 100 wartości z przedziału 0-200 korzystając z klasy Random.
 * Program po losowaniu zlicza powtórzenia każdej z liczb i wyświetla 5 liczb które
 * najczęściej się powtarzają w kolejności malejącej.
 */


public class RadndomCounter {


//picker() zwraca liste ze 100 wartosciami od 0 do 200.
    static ArrayList<Integer> picker() {
        ArrayList<Integer> input = new ArrayList<Integer>();
        Random random = new Random();
        while (input.size() < 100) {                           //warunek ustalajacy ilsoc losowan i rozmiar listy.
            input.add(random.nextInt(20 - 0) + 0);     //zakres losowania.
        }
        return input;
    }



//oftenFive() pobiera jako arguent liste Int, wpisuje
    static void oftenFive(ArrayList<Integer> input){
// HashMapa howOften (key to wylosowana liczna a value to ilosc powtorzen):
        HashMap<Integer, Integer> howOften = new HashMap<Integer, Integer>();
        for (int i = 0; i < input.size(); i++) {
            int counter = 0;
            for (int j = 0; j < input.size(); j++) {
                if (input.get(i) == input.get(j)) {
                    counter++;
                }
            }
            howOften.put(input.get(i),counter);
        }

        System.out.println("Zestawienie wyslosowanych liczb wraz z powtorzeniami: ");
        System.out.println(howOften);
        System.out.println();

//stream wyszukujacy 5 najczesciej powtarzajacych sie liczb w kolejnosci malejacej.
        howOften.entrySet()
                .stream()
                .sorted(HashMap.Entry.comparingByValue(Comparator.reverseOrder()))
                .limit(5).forEach(x -> System.out.println("Najczesciej powtarzajace sie liczby to: "+ x+"razy."));

    }




    public static void main(String[] args) {
        oftenFive(picker());
    }
}
