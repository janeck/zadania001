package co.uk.zadania_lukasz_001;


/**
 Przygotuj program losujący 20 liczb całkowitych. Program musi pamiętać wylosowane
 liczby i jeśli liczba wylosowana się powtórzyła następuje ponowne losowanie,
 aż do wylosowania liczby unikalnej
 */



import java.util.HashSet;
import java.util.Random;

public class randomOfTwenty {
    public static void main(String[] args) {

        HashSet<Integer> set = new HashSet();
        Random random = new Random();
        int count = 0;

        while (set.size() < 20) {
            set.add(random.nextInt(40));
            count++;
        }

        System.out.println(set);
        System.out.println("Liczbe losowano :" + count + " razy.");
    }
}
