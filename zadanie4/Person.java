package co.uk.zadania_lukasz_001.zadanie4;

import java.util.StringJoiner;

/**
 4.Przygotuj klasę osoba z polami imię, nazwisko, PESEL oraz nadpisz(@override) metodę .toString()
 taka by wyświetlała dan eosoby w formacie "Imie:%s, Nazwisko:%s, PESEL:%s".
 Utwórz instancję klasy osoba, wypełnij przykładowymi danymi oraz wyświetl
 dane osoby korzystając z przygotowanej metody .toString()
 */


public class Person {
    private String name;
    private String surname;
    private String pesel;

    public Person(String name, String surname, String pesel) {
        this.name = name;
        this.surname = surname;
        this.pesel = pesel;
    }

    @Override
    public String toString() {
        return String.format("Imie:%s, Nazwisko:%s, PESEL:%s", this.name, this.surname, this.pesel);
    }
}
